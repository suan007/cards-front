import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { RoutePath } from '../../app/App';
import { AppRootStateType } from '../../app/store';
import { AddItemForm } from '../../components/AddItemForm/AddItemForm';
import { getCardsTC } from '../Cards/cardsReducer';
import { Pack } from './pack';
import { addCardsPack, deleteCardsPackTC, getPacks, GetPacksAPIParamsType, PacksType, setSearchPackName } from './packsReducer';

type PacksPropsType = {

}
export const Packs: React.FC<PacksPropsType> = () => {
    const dispatch = useDispatch()
    const packs = useSelector<AppRootStateType, PacksType[]>(state => state.packs.packsList)
    const isLogged = useSelector<AppRootStateType, boolean>(state => state.login.logged)
    const params = useSelector<AppRootStateType, GetPacksAPIParamsType>(state => state.packs.packsParams)

    useEffect(() => {
        if (isLogged) {
            dispatch(getPacks())
        }
    }, [isLogged, params]);

    const addCardsPackHandler = (title: string, value: boolean) => {
        dispatch(addCardsPack(title, value))
    }

    const searchCardsPackHandler = (title: string, value: boolean) => {
        dispatch(setSearchPackName(title))
    }
    return <div style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column"
    }}>
        <div>
            <AddItemForm addItem={addCardsPackHandler} placeholder={"add"} />
            <AddItemForm addItem={searchCardsPackHandler} placeholder={"search"} />
        </div>
        <table style={{ border: '1px solid blue' }}>
            <tr>
                <th style={{ border: '1px solid blue' }}>Name</th>
                <th style={{ border: '1px solid blue' }}>Cads count</th>
                <th style={{ border: '1px solid blue' }}>User name</th>
                <th style={{ border: '1px solid blue' }}>Updated</th>
            </tr>
            {
                packs.map(pack => <Pack pack={pack} />)
            }


        </table>

    </div>

}

