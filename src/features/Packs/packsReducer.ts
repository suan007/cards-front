import { AddCardsPackDataType } from './../../api/packsApi'
import { setAppStatus } from '../../app/appReducer'
import { ForgotResponceType } from '../../api/autApi'
import React from 'react'
import { authAPI, LoginRequestType, LoginResponceType } from '../../api/autApi'
import { AppThunkType, GetAppStateType } from '../../app/store'
import { packsAPI } from '../../api/packsApi'

const initState = {
  packsList: [] as PacksType[],
  packsParams: {
    min: 0,
    max: 20,
    page: 1,
    pageCount: 15,
    sortPacks: '0updated',
    packName: '',
  } as GetPacksAPIParamsType,
}

export const paksReducer = (
  state: PacksInitStateType = initState,
  action: PacksActionType
): PacksInitStateType => {
  switch (action.type) {
    case packsReducerActions.SET_PACKS:
      return { ...state, packsList: [...action.payload] }
    case packsReducerActions.SET_SEARCH_PACK_NAME:
      return {
        ...state,
        packsParams: { ...state.packsParams, packName: action.title },
      }
    default:
      return state
  }
}

// <--------------------------- acion creators ------------------------------>

//consts
export const packsReducerActions = {
  SET_PACKS: 'PACKS/SET_PACKS',
  SET_SEARCH_PACK_NAME: 'PACKS/SET_SEARCH_PACK_NAME',
} as const

export const setPacks = (payload: PacksType[]) =>
  ({
    type: packsReducerActions.SET_PACKS,
    payload,
  } as const)

export const setSearchPackName = (title: string) =>
  ({
    type: packsReducerActions.SET_SEARCH_PACK_NAME,
    title,
  } as const)

// <--------------------------- thunk creators ------------------------------>
export const getPacks =
  (): AppThunkType => async (dispatch, getState: GetAppStateType) => {
    dispatch(setAppStatus('loading'))
    const params = getState().packs.packsParams
    try {
      const responce = await packsAPI.getPacks(params)
      dispatch(setPacks(responce.data.cardPacks))
      dispatch(setAppStatus('success'))
    } catch (e) {
      const error = e.responce
        ? e.responce.data.error
        : e.message + ', more details in the console'
      console.log('error:', { ...e })
      dispatch(setAppStatus('failed'))
    }
  }

export const addCardsPack =
  (packName: string, isPrivat: boolean): AppThunkType =>
  async dispatch => {
    dispatch(setAppStatus('loading'))
    const payload = {
      cardsPack: {
        deckCover: '',
        grade: 0,
        name: packName,
        path: '',
        privat: isPrivat,
        rating: 0,
        shots: 0,
        type: 'TestPack',
      },
    }
    try {
      const responce = await packsAPI.addCardsPack(payload)
      dispatch(getPacks())
      dispatch(setAppStatus('success'))
    } catch (e) {
      const error = e.responce
        ? e.responce.data.error
        : e.message + ', more details in the console'
      console.log('error:', { ...e })
      dispatch(setAppStatus('failed'))
    }
  }

export const deleteCardsPackTC =
  (packId: string): AppThunkType =>
  async dispatch => {
    dispatch(setAppStatus('loading'))

    try {
      const responce = await packsAPI.deleteCardsPack(packId)
      dispatch(getPacks())
      dispatch(setAppStatus('success'))
    } catch (e) {
      const error = e.responce
        ? e.responce.data.error
        : e.message + ', more details in the console'
      console.log('error:', { ...e })
      dispatch(setAppStatus('failed'))
    }
  }

export const updateCardsPackTC =
  (packName: string, id: string): AppThunkType =>
  async dispatch => {
    dispatch(setAppStatus('loading'))
    const payload = {
      cardsPack: {
        _id: id,
        name: packName,
      },
    }
    try {
      const responce = await packsAPI.updateCardsPack(payload)
      dispatch(getPacks())
      dispatch(setAppStatus('success'))
    } catch (e) {
      const error = e.responce
        ? e.responce.data.error
        : e.message + ', more details in the console'
      console.log('error:', { ...e })
      dispatch(setAppStatus('failed'))
    }
  }
// <--------------------------- types ------------------------------>

export type PacksActionType =
  | ReturnType<typeof setPacks>
  | ReturnType<typeof setSearchPackName>

type PacksInitStateType = typeof initState

export type PacksType = {
  _id: string
  user_id: string
  name: string
  path: string // папка
  cardsCount: number
  grade: number // средняя оценка карточек
  shots: number // количество попыток
  rating: number // лайки
  type: 'pack' // ещё будет "folder" (папка)
  created: Date
  updated: Date
  __v: number
}

export type GetPacksAPIParamsType = {
  packName?: string
  min?: number
  max?: number
  sortPacks?: string
  page?: number
  pageCount?: number
  user_id?: string
}
