import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { RoutePath } from '../../app/App';
import { AppRootStateType } from '../../app/store';
import { AddItemForm } from '../../components/AddItemForm/AddItemForm';
import { getCardsTC } from '../Cards/cardsReducer';
import { addCardsPack, deleteCardsPackTC, getPacks, PacksType, updateCardsPackTC } from './packsReducer';

type PackPropsType = {
    pack: PacksType
}
export const Pack: React.FC<PackPropsType> = ({ pack }) => {
    const dispatch = useDispatch()
    const [editMode, setEditMode] = useState(true)
    const [title, setTitle] = useState(pack.name)

    const getCards = () => {
        dispatch(getCardsTC(pack._id))
    }
    const deletePack = () => {
        dispatch(deleteCardsPackTC(pack._id))
    }
    const updatePack = () => {
        let trimmed = title.trim()
        if (trimmed) {
            dispatch(updateCardsPackTC(trimmed, pack._id))
        }
        setEditMode(!editMode)
    }
    return <div>

        return <tr>

            <td style={{ border: '1px solid blue' }}>{editMode ? pack.name : <input value={title} onChange={(e) => setTitle(e.currentTarget.value)} />}</td>
            <td style={{ border: '1px solid blue' }}>{pack.cardsCount}</td>
            <td style={{ border: '1px solid blue' }}>no name</td>
            <td style={{ border: '1px solid blue' }}>{pack.updated}</td>
            <td ><NavLink to={`/cards-list/${pack._id}`}>cards</NavLink></td>
            <button onClick={deletePack}>delete</button>
            <button onClick={updatePack}>{editMode ? "update" : "save"}</button>
        </tr>

    </div>

}

