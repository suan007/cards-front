import React from 'react'
import { authAPI } from '../../api/autApi'
import { AppThunkType } from '../../app/store'
import { AddNewPasswordRequestType } from './addNewPassword'

export const resetPassActions = {
  IS_LOADING: 'IS_LOADING',
  SET_ERROR: 'SET_ERROR',
  SET_FORGOT_REQUEST_STATUS: 'SET_FORGOT_REQUEST_STATUS',
} as const

export type ForgotRequestStatusType = "idle" | "sentToMail" | "success" 
const initState = {
  loadingRequest: false,
  validationMessage: '',
  forgotRequestStatus: "idle" as ForgotRequestStatusType,
}

export const isLoading = (isLoading: boolean) =>
  ({
    type: resetPassActions.IS_LOADING,
    isLoading,
  } as const)

export const setvalidationMessage = (validationMessage: string) =>
  ({
    type: resetPassActions.SET_ERROR,
    validationMessage,
  } as const)

export const setForgotRequestStatus = (status: ForgotRequestStatusType) =>
  ({
    type: resetPassActions.SET_FORGOT_REQUEST_STATUS,
    status,
  } as const)

export const resetPassReducer = (
  state: ResetPassInitStateType = initState,
  action: ResetPassActionType
): ResetPassInitStateType => {
  switch (action.type) {
    case "IS_LOADING":
      return { ...state, loadingRequest: action.isLoading }
    case "SET_ERROR":
      return { ...state, validationMessage: action.validationMessage }
    case "SET_FORGOT_REQUEST_STATUS":
      return { ...state, forgotRequestStatus: action.status }
    default:
      return state
  }
}

export const forgotPassTC =
  (email: string): AppThunkType =>
  async dispatch => {
    dispatch(isLoading(true))
    try {
      const responce = await authAPI.forgotPass(email)
      dispatch(setvalidationMessage(responce.data.info))
      dispatch(setForgotRequestStatus("sentToMail"))
    } catch (e) {
      const error = e.responce
        ? e.responce.data.error
        : e.message + ', more details in the console'
      console.log('error:', { ...e })
      dispatch(setvalidationMessage(error))
    } finally {
      dispatch(isLoading(false))
    }
  }

export const resetPassTC =
  (payload: AddNewPasswordRequestType): AppThunkType =>
  async dispatch => {
    dispatch(isLoading(true))
    try {
      const responce = await authAPI.resetPass(payload)
      dispatch(setvalidationMessage(responce.data.info))
      dispatch(setForgotRequestStatus("success"))
    } catch (e) {
      const error = e.responce
        ? e.responce.data.error
        : e.message + ', more details in the console'
      console.log('error:', { ...e })
      dispatch(setvalidationMessage(error))
    } finally {
      dispatch(isLoading(false))
    }
  }

// types
type isLoadingAT = ReturnType<typeof isLoading>
type setErrorAT = ReturnType<typeof setvalidationMessage>
type setForgotRequestStatusAT = ReturnType<typeof setForgotRequestStatus>
type ResetPassInitStateType = typeof initState
export type ResetPassActionType =
  | isLoadingAT
  | setErrorAT
  | setForgotRequestStatusAT
//
