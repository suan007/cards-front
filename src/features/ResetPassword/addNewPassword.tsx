import { useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { Redirect, useParams } from "react-router-dom"
import { RoutePath } from "../../app/App"
import { AppRootStateType } from "../../app/store"
import SuperInputText from "../../components/common/c1-SuperInputText/SuperInputText"
import SuperButton from "../../components/common/c2-SuperButton/SuperButton"
import { ForgotRequestStatusType, resetPassTC } from "./resetPassReducer"

export type AddNewPasswordRequestType = {
    password: string,
    resetPasswordToken: string
}
export const AddNewPassword = () => {
    const validationMessage = useSelector<AppRootStateType, string>(state => state.resetPass.validationMessage)
    const forgotRequestStatus = useSelector<AppRootStateType, ForgotRequestStatusType>(state => state.resetPass.forgotRequestStatus)
    const { resetPasswordToken = "" } = useParams<{ resetPasswordToken: string }>();
    const dispatch = useDispatch()
    const [password, setFirstPass] = useState("")
    const [secondPass, setSecondPass] = useState("")

    const onClickHandler = () => {
        dispatch(resetPassTC({ password, resetPasswordToken }))
    }

    if (forgotRequestStatus === "success" ) {
        return <Redirect to={RoutePath.LOGIN} />
    }
    return <>
        <div style={{ height: "15px", padding: "10px" }}>
            {
                validationMessage && validationMessage
            }
        </div>
        <SuperInputText value={password} onChangeText={setFirstPass} />
        <SuperInputText value={secondPass} onChangeText={setSecondPass} />
        <SuperButton title="reset" onClick={onClickHandler} disabled={password !== secondPass} />
    </>
}
