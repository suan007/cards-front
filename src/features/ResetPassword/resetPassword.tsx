import { useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { NavLink, Redirect } from "react-router-dom"
import { RoutePath } from "../../app/App"
import { AppRootStateType } from "../../app/store"
import SuperInputText from "../../components/common/c1-SuperInputText/SuperInputText"
import SuperButton from "../../components/common/c2-SuperButton/SuperButton"
import { forgotPassTC, ForgotRequestStatusType } from "./resetPassReducer"

export const ResetPassword = () => {
    const forgotRequestStatus = useSelector<AppRootStateType, ForgotRequestStatusType>(state => state.resetPass.forgotRequestStatus)
    const dispatch = useDispatch();
    const [email, setEmail] = useState("")
    const onClickHandler = () => {
        dispatch(forgotPassTC(email))
    }
    const validationMessage = useSelector<AppRootStateType, string>(state => state.resetPass.validationMessage)
    return <>
        <div style={{ height: "15px", padding: "5px" }}>
            {validationMessage && validationMessage}
            {forgotRequestStatus === "sentToMail" && "check email"}
        </div>
        <SuperInputText value={email} onChangeText={setEmail} />
        <SuperButton title="send" onClick={onClickHandler} />
        <NavLink to={RoutePath.LOGIN}>Login</NavLink>
    </>
}


