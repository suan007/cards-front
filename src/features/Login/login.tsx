import { useFormik } from 'formik';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { NavLink, Redirect } from 'react-router-dom';
import { RoutePath } from '../../app/App';
import { AppRootStateType } from '../../app/store';
import SuperInputText from '../../components/common/c1-SuperInputText/SuperInputText';
import SuperButton from '../../components/common/c2-SuperButton/SuperButton';
import SuperCheckbox from '../../components/common/c3-SuperCheckbox/SuperCheckbox';
import { PreloaderSmall } from '../../components/Preloader/Preloader';
import { loginTC } from './loginReducer';

type FormErrorType = {
    email?: string
    password?: string
    rememberMe?: boolean
}
export const Login = () => {
    const dispatch = useDispatch();
    const validation = useSelector<AppRootStateType, string>(state => state.login.requestValidation)
    const isLogged = useSelector<AppRootStateType, boolean>(state => state.login.logged)
    const formik = useFormik({
        initialValues: {
            email: '',
            password: '',
            rememberMe: false,
        },
        validate: (values) => {
            const errors: FormErrorType = {};
            if (!values.email) {
                errors.email = 'Required';
            } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
                errors.email = 'Invalid email address'
            }
            if (!values.password) {
                errors.password = 'Required';
            } else if (values.password.length < 7) {
                errors.password = 'Must be 7 characters at least';
            }
            return errors;
        },
        onSubmit: values => {
            const email = values.email
            const password = values.password
            const rememberMe = values.rememberMe
            formik.resetForm()
            dispatch(loginTC({ email, password, rememberMe }))
        },
    })
    if (isLogged) {
        return <Redirect to={RoutePath.PROFILE} />
    }

    return <>

        <form onSubmit={formik.handleSubmit}>
            <div style={{ height: "15px" }}>
                {validation}
            </div>
            <div>
                <span>Email</span>
                {formik.errors.email}
                <SuperInputText
                    {...formik.getFieldProps('email')}
                />
            </div>
            <div>
                <span>Password</span>
                {formik.errors.password}
                <SuperInputText type={"password"}
                    {...formik.getFieldProps('password')}
                />
            </div>

            <div>
                <span>Remember me</span>
                <SuperCheckbox
                    {...formik.getFieldProps('rememberMe')}
                />
            </div>
            <div >
                <NavLink to={RoutePath.RESET_PASSWORD}>Forgot Password?</NavLink>
            </div>
            <SuperButton title="Login" />

        </form>
    </>

}