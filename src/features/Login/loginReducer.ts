import { setAppStatus } from './../../app/appReducer'
import { ForgotResponceType } from './../../api/autApi'
import React from 'react'
import { authAPI, LoginRequestType, LoginResponceType } from '../../api/autApi'
import { AppThunkType } from '../../app/store'

const initState = {
  userLoginData: {} as LoginResponceType ,
  logged: false,
  requestValidation: '',
}

export const loginReducer = (
  state: LoginInitState = initState,
  action: LoginActionType
): LoginInitState => {
  switch (action.type) {
    case loginReducerActions.SET_USER_LOGIN_DATA:
      return { ...state, userLoginData: action.payload, logged: action.logged }
    case loginReducerActions.LOGOUT:
      return { ...state, logged: false }
    case loginReducerActions.SET_VALIDATION:
      return { ...state, requestValidation: action.message }
    default:
      return state
  }
}

// <--------------------------- acion creators ------------------------------>

//consts
export const loginReducerActions = {
  SET_USER_LOGIN_DATA: 'SET_USER_LOGIN_DATA',
  LOGOUT: 'LOGOUT',
  SET_VALIDATION: 'LOGIN/SET_VALIDATION',
} as const

export const loginAC = (payload: LoginResponceType, logged: boolean) =>
  ({
    type: loginReducerActions.SET_USER_LOGIN_DATA,
    payload,
    logged,
  } as const)

export const logoutAC = () =>
  ({
    type: loginReducerActions.LOGOUT,
  } as const)

export const setValidationLogin = (message: string) =>
  ({
    type: loginReducerActions.SET_VALIDATION,
    message,
  } as const)

// <--------------------------- thunk creators ------------------------------>
export const loginTC =
  (payload: LoginRequestType): AppThunkType =>
  async dispatch => {
    dispatch(setAppStatus('loading'))
    try {
      const responce = await authAPI.login(payload)
      dispatch(loginAC(responce.data, true))
      dispatch(setAppStatus('success'))
    } catch (e) {
      const error = e.responce
        ? e.responce.data.error
        : e.message + ', more details in the console'
      dispatch(setValidationLogin(error))
      console.log('error:', { ...e })
      dispatch(setAppStatus('failed'))
    }
  }

export const logoutTC = (): AppThunkType => async dispatch => {
  try {
    await authAPI.logout()
    dispatch(logoutAC())
  } catch (e) {
    const error = e.responce
      ? e.responce.data.error
      : e.message + ', more details in the console'
    console.log('error:', { ...e })
  }
}

export const authMeTC = (): AppThunkType => async dispatch => {
  dispatch(setAppStatus('loading'))
  try {
    const responce = await authAPI.authMe()
    dispatch(loginAC(responce.data, true))
    dispatch(setAppStatus('success'))
  } catch (e) {
    const error = e.responce
      ? e.responce.data.error
      : e.message + ', more details in the console'
    console.log('error:', { ...e })
    dispatch(setAppStatus('failed'))
  }
}

// <--------------------------- types ------------------------------>

export type LoginActionType = LoginAT | LogoutAT | SetValidationLoginType
type SetValidationLoginType = ReturnType<typeof setValidationLogin>
type LoginAT = ReturnType<typeof loginAC>
type LogoutAT = ReturnType<typeof logoutAC>
type LoginInitState = typeof initState
