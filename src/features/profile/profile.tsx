import { useSelector } from "react-redux"
import { LoginResponceType } from "../../api/autApi"
import { AppRootStateType } from "../../app/store"
import { RedirectToLogin } from "../../components/RedirecToLogin"

export const Profile = () => {
    const profileInfo = useSelector<AppRootStateType, LoginResponceType>(state => state.login.userLoginData)

    return <>
        <RedirectToLogin />
        Profile Page

        <div>
            <img style={{ width: "100px" }} src={profileInfo.avatar} alt="" />
            <div>
                {profileInfo.name}
            </div>
        </div>


    </>
}
