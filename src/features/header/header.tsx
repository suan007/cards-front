import React from "react";
import SuperButton from "../../components/common/c2-SuperButton/SuperButton";
import { NavLink } from "react-router-dom";
import s from "./header.module.css"
import { RoutePath } from "../../app/App";
import { useDispatch, useSelector } from "react-redux";
import { AppRootStateType } from "../../app/store";
import { logoutTC } from "../Login/loginReducer";


export const Header = () => {
    const dispatch = useDispatch()
    const logged = useSelector<AppRootStateType, boolean>(state => state.login.logged)
    const onClickHandler = () => {
        dispatch(logoutTC())

    }
    return <div className={s.header}>
        <NavLink to={RoutePath.REGISTRATION}><SuperButton title={'registration'} /></NavLink>
        {
            logged
                ? <NavLink to={RoutePath.LOGIN}><SuperButton title={'logout'} onClick={onClickHandler} /></NavLink>
                : <NavLink to={RoutePath.LOGIN}><SuperButton title={'login'} /></NavLink>
        }
        <NavLink to={RoutePath.RESET_PASSWORD}><SuperButton title={'resetPass'} /></NavLink>
        <NavLink to={RoutePath.ADD_NEW_PASSWORD}><SuperButton title={'newPass'} /></NavLink>
        <NavLink to={RoutePath.PROFILE}><SuperButton title={'profile'} /></NavLink>
        <NavLink to={RoutePath.PACKS}><SuperButton title={'packs'} /></NavLink>
        <NavLink to={RoutePath.CARDS}><SuperButton title={'cards-list'} /></NavLink>
    </div>
}
