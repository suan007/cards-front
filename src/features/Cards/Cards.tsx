import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { CardType } from '../../api/packsApi';
import { AppRootStateType } from '../../app/store';
import { AddItemForm } from '../../components/AddItemForm/AddItemForm';
import { addCardTC, getCardsTC } from './cardsReducer';


type CardsPropsType = {

}
export const Cards: React.FC<CardsPropsType> = () => {
    const cards = useSelector<AppRootStateType, CardType[]>(state => state.cards.cards)
    const { packId = "" } = useParams<{ packId: string }>();
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(getCardsTC(packId))
    }, [dispatch, packId]);
    const addCardHandler = (title: string, value: boolean, title2: string) => {
        dispatch(addCardTC(packId, title, title2))
    }
    return <div style={{ display: "flex", alignItems: "center", justifyContent: "center", flexDirection: "column" }}>
        <AddItemForm addItem={addCardHandler} placeholder={"add"} isTwo />

        <table style={{ border: '1px solid blue' }}>
            <tr>
                <th style={{ border: '1px solid blue' }}>Question</th>
                <th style={{ border: '1px solid blue' }}>answer</th>
                <th style={{ border: '1px solid blue' }}>grade</th>
                <th style={{ border: '1px solid blue' }}>rating</th>
            </tr>
            {
                cards.map(card => {

                    return <tr key={card._id}>
                        <td style={{ border: '1px solid blue' }}>{card.question}</td>
                        <td style={{ border: '1px solid blue' }}>{card.answer}</td>
                        <td style={{ border: '1px solid blue' }}>{card.grade}</td>
                        <td style={{ border: '1px solid blue' }}>{card.rating}</td>
                    </tr>
                })


            }


        </table>

    </div>

}