import { cardsAPI, CardType, PostCardDataType } from './../../api/packsApi'
import { setAppStatus } from '../../app/appReducer'
import { ForgotResponceType } from '../../api/autApi'
import React from 'react'
import { authAPI, LoginRequestType, LoginResponceType } from '../../api/autApi'
import { AppThunkType } from '../../app/store'

const initState: CardsInitStateType = {
  cards: [],
}

export const cardsReducer = (
  state: CardsInitStateType = initState,
  action: CardsActionType
): CardsInitStateType => {
  switch (action.type) {
    case cardsActions.SET_CARDS:
      return {
        ...state,
        cards: action.payload,
      }
    default:
      return state
  }
}

// <--------------------------- acion creators ------------------------------>

//consts
export const cardsActions = {
  SET_CARDS: 'cards/SET_CARDS',
} as const

export const setCards = (payload: CardType[]) =>
  ({
    type: cardsActions.SET_CARDS,
    payload,
  } as const)

// <--------------------------- thunk creators ------------------------------>
export const getCardsTC =
  (packId: string): AppThunkType =>
  async dispatch => {
    dispatch(setAppStatus('loading'))
    try {
      const responce = await cardsAPI.getCards(packId)
      dispatch(setCards(responce.data.cards))
      dispatch(setAppStatus('success'))
    } catch (e) {
      const error = e.responce
        ? e.responce.data.error
        : e.message + ', more details in the console'
      console.log('error:', { ...e })
      dispatch(setAppStatus('failed'))
    }
  }

export const addCardTC =
  (
    packId: string,
    question: string,
    answer: string
  ): AppThunkType =>
  async dispatch => {
    dispatch(setAppStatus('loading'))
    let payload = {
      card: {
        cardsPack_id: packId,
        question: question,
        answer: answer,
        type: 'card',
      },
    } as PostCardDataType
    try {
      const responce = await cardsAPI.addCard(payload)
      dispatch(getCardsTC(packId))
      dispatch(setAppStatus('success'))
    } catch (e) {
      const error = e.responce
        ? e.responce.data.error
        : e.message + ', more details in the console'
      console.log('error:', { ...e })
      dispatch(setAppStatus('failed'))
    }
  }
// <--------------------------- types ------------------------------>

export type CardsActionType = ReturnType<typeof setCards>

type CardsInitStateType = {
  cards: CardType[]
}
