import { setAppStatus } from './../../app/appReducer'
import { Dispatch } from 'redux'
import { authAPI } from '../../api/autApi'
import { AppThunkType } from '../../app/store'

export const authActions = {
  SIGN_UP: 'AUTH/SIGN_UP',
  SET_VALID_MESSAGE: 'SET_VALID_MESSAGE',
} as const

export type AuthActionsType = SignUpAT | SetRegisValidationMessage
const initState = {
  userRegistrationData: {
    email: ('' as string) || null,
    password: ('' as string) || null,
  },
  validationMessage: '',
}

// types
type SignUpAT = ReturnType<typeof signUpAC>
type SetRegisValidationMessage = ReturnType<typeof setRegisValidationMessage>
type AuthInitialStateType = typeof initState

//

export const authReducer = (
  state: AuthInitialStateType = initState,
  action: AuthActionsType
): AuthInitialStateType => {
  switch (action.type) {
    case authActions.SIGN_UP:
      return {
        ...state,
        userRegistrationData: {
          email: action.email,
          password: action.password,
        },
      }
    case authActions.SET_VALID_MESSAGE:
      return {
        ...state,
        validationMessage: action.message,
      }
    default:
      return state
  }
}

export const signUpAC = (emailValue: string, passwordValue: string) =>
  ({
    type: authActions.SIGN_UP,
    email: emailValue,
    password: passwordValue,
  } as const)

export const setRegisValidationMessage = (message: string) =>
  ({
    type: authActions.SET_VALID_MESSAGE,
    message,
  } as const)

export const singUp =
  (email: string, password: string): AppThunkType =>
  async dispatch => {
    dispatch(setAppStatus('loading'))
    try {
      const responce = await authAPI.registration({ email, password })
      dispatch(signUpAC(email, password))
      dispatch(setAppStatus('success'))
    } catch (e) {
      const error = e.responce
        ? e.responce.data.error
        : e.message + ', more details in the console'
      console.log('error:', { ...e })
      dispatch(setAppStatus('failed'))
      dispatch(setRegisValidationMessage(error))
    }
  }
