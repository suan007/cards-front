import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { singUp } from './registrationReducer';
import { AppRootStateType } from '../../app/store';
import SuperInputText from '../../components/common/c1-SuperInputText/SuperInputText';
import SuperButton from '../../components/common/c2-SuperButton/SuperButton';
import { RoutePath } from '../../app/App';

export const Registration = () => {
    const dispatch = useDispatch()
    const isRegistered = useSelector<AppRootStateType, string | null>(state => state.auth.userRegistrationData.email)
    const validationMessage = useSelector<AppRootStateType, string>(state => state.auth.validationMessage)

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [checkPassword, setCheckPassword] = useState('')

    const onCkickHandler = () => {
        if (password === checkPassword) {
            dispatch(singUp(email, password))
        }
    }
    if (isRegistered) {
        return <Redirect to={RoutePath.LOGIN} />
    }
    return (
        <div>
            <div style={{ height: "15px", padding: "10px" }}>
                {validationMessage && validationMessage}
            </div>
            <SuperInputText value={email} placeholder={'Email'} onChangeText={setEmail}></SuperInputText>
            <SuperInputText value={password} placeholder={'Password'} onChangeText={setPassword}></SuperInputText>
            <SuperInputText value={checkPassword} placeholder={'Password'} onChangeText={setCheckPassword}></SuperInputText>
            <SuperButton onClick={onCkickHandler} title={'sign up'} />
        </div>
    )
}
