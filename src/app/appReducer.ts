import React from 'react'

export const appReducerActions = {
  SET_APP_STATUS: "SET_APP_STATUS"
} as const
export type AppStatusType = "idle" | "loading" | "success" | "failed"
export type AppActionType = SetAppStatusAT 
const initState = {
 appStatus: "idle" as AppStatusType,
}

// types
type SetAppStatusAT = ReturnType<typeof setAppStatus>
type AppInitStateType = typeof initState

//

export const appReducer = (
  state: AppInitStateType = initState,
  action: AppActionType
): AppInitStateType => {
  switch (action.type) {
   
    default:
      return state
  }
}

export const setAppStatus = (status: AppStatusType) =>
  ({
    type: appReducerActions.SET_APP_STATUS,
    status
  } as const)



