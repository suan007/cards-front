import { CardsActionType, cardsReducer } from './../features/Cards/cardsReducer'
import { applyMiddleware, combineReducers, compose, createStore } from 'redux'
import thunk, { ThunkAction } from 'redux-thunk'
import {
  AuthActionsType,
  authReducer,
} from '../features/Registration/registrationReducer'
import { LoginActionType, loginReducer } from '../features/Login/loginReducer'
import {
  ResetPassActionType,
  resetPassReducer,
} from '../features/ResetPassword/resetPassReducer'
import { AppActionType, appReducer } from './appReducer'
import { PacksActionType, paksReducer } from '../features/Packs/packsReducer'
import { profileReducer } from '../features/profile/profile-reducer'

const rootReducer = combineReducers({
  app: appReducer,
  auth: authReducer,
  login: loginReducer,
  resetPass: resetPassReducer,
  profile: profileReducer,
  cards: cardsReducer,
  packs: paksReducer,
})
export type AppRootStateType = ReturnType<typeof rootReducer>
export type AppActionsType =
  | AuthActionsType
  | LoginActionType
  | ResetPassActionType
  | AppActionType
  | CardsActionType
  | PacksActionType

export type AppThunkType<ReturnType = void> = ThunkAction<
  ReturnType,
  AppRootStateType,
  unknown,
  AppActionsType
>

export type GetAppStateType = () => AppRootStateType 
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
export const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(thunk))
)

declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose
  }
}
