import React, { useEffect } from 'react';
import './App.css';
import { HashRouter, Redirect, Route, Switch, useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { Registration } from '../features/Registration/registration';
import { ResetPassword } from '../features/ResetPassword/resetPassword';
import { AddNewPassword } from '../features/ResetPassword/addNewPassword';
import { Profile } from '../features/profile/profile';
import { NotFound } from '../components/notFound/notFound';
import { Header } from '../features/header/header';
import { Login } from '../features/Login/login';
import { authMeTC } from '../features/Login/loginReducer';
import { AppRootStateType } from './store';
import { AppStatusType } from './appReducer';
import { PreloaderSmall } from '../components/Preloader/Preloader';
import { RedirectToLogin } from '../components/RedirecToLogin';
import { Cards } from '../features/Cards/Cards';
import { Packs } from '../features/Packs/Packs';


export const RoutePath = {
  REGISTRATION: "/registration",
  LOGIN: "/login",
  RESET_PASSWORD: "/resetpassword",
  ADD_NEW_PASSWORD: "/set-new-password/:resetPasswordToken",
  PROFILE: "/profile",
  CARDS: "/cards-list/:packId",
  PACKS: "/packs"

}

function App() {
  console.log("render");
  const dispatch = useDispatch()
  const appStatus = useSelector<AppRootStateType, AppStatusType>(state => state.app.appStatus)
  const logged = useSelector<AppRootStateType, boolean>(state => state.login.logged)


  useEffect(() => {
    if (!logged) {
      dispatch(authMeTC())
    }
  })

  return (
    <div className="App">
      <HashRouter>
        <Header />
        {
          appStatus === "loading"
            ? <PreloaderSmall />
            : <Switch>
              <Route path={RoutePath.REGISTRATION} render={() => <Registration />} />
              <Route path={RoutePath.LOGIN} render={() => <Login />} />
              <Route path={RoutePath.RESET_PASSWORD} render={() => <ResetPassword />} />
              <Route path={RoutePath.ADD_NEW_PASSWORD} render={() => <AddNewPassword />} />
              <Route path={RoutePath.PROFILE} render={() => <Profile />} />
              <Route path={RoutePath.CARDS} render={() => <Cards />} />
              <Route path={RoutePath.PACKS} render={() => <Packs />} />
              <Route path={"/404"} render={() => <NotFound />} />
              <Redirect from={"*"} to={"/404"} />
              <Route path={"/"} exact render={() => <Profile />} />
            </Switch>
        }
      </HashRouter>
    </div>
  );
}

export default App;
