import axios from 'axios'
import { AddNewPasswordRequestType } from '../features/ResetPassword/addNewPassword'

export const instance = axios.create({
  baseURL: 'https://neko-back.herokuapp.com/2.0/',
  withCredentials: true,
})

export const authAPI = {
  authMe() {
    return instance.post<any>('auth/me', {})
  },
  registration(payload: userRegType) {
    return instance.post<AuthAPIResponseType>('auth/register', payload)
  },
  login(payload: LoginRequestType) {
    return instance.post<LoginResponceType>('auth/login', { ...payload })
  },
  logout() {
    return instance.delete<ForgotResponceType>('auth/me', {})
  },
  forgotPass(email: string) {
    return instance.post<ForgotResponceType>(
      'auth/forgot',
      forgotRequestData(email)
    )
  },
  resetPass(payload: AddNewPasswordRequestType) {
    return instance.post<ForgotResponceType>('auth/set-new-password', payload)
  },
}


//utils
const forgotRequestData = (email: string) => ({
  email,
  from: 'test-front-admin <abzal008@mail.ru>',
  message: `
  <div style="background-color: lime; padding: 15px">
       password recovery link:
       <a href='https://hj-abzal.github.io/cards-front/#/set-new-password/$token$'>
        link
      </a>
  </div>`,
})

// types
export type userRegType = {
  email: string
  password: string
}
export type AuthAPIResponseType = {
  addedUser: any
  error?: string
}

export type LoginRequestType = {
  email: string
  password: string
  rememberMe: boolean
}
export type LoginResponceType = {
  _id: string
  email: string
  name: string
  avatar: string
  publicCardPacksCount: number
  created: Date
  updated: Date
  isAdmin: boolean
  verified: boolean
  rememberMe: boolean
  error?: string
}
export type ForgotResponceType = {
  info: string
  error: string
}
