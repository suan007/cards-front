import axios from 'axios'
import { GetPacksAPIParamsType } from '../features/Packs/packsReducer'
import { instance } from './autApi'

export const packsAPI = {
  getPacks(params: GetPacksAPIParamsType) {
    return instance.get('cards/pack', {
      params: {
        packName: params.packName,
        min: params.min,
        max: params.max,
        sortPacks: params.sortPacks,
        page: params.page,
        pageCount: params.pageCount,
      },
    })
  },
  addCardsPack(payload: AddCardsPackDataType) {
    return instance.post('cards/pack', payload)
  },
  deleteCardsPack(id: string) {
    return instance.delete(`cards/pack?id=${id}`)
  },
  updateCardsPack(payload: UpdateCardsPackDataType) {
    return instance.put('cards/pack', payload)
  },
}

export const cardsAPI = {
  getCards(packId: string) {
    return instance.get<GetDataType>(`cards/card?cardsPack_id=${packId}`)
  },
  addCard(payload: PostCardDataType) {
    return instance.post<PostCardDataType>('cards/card', payload)
  },
}

export type GetDataType = {
  cards: CardType[]

  error: string
}

export type CardType = {
  _id: string
  cardsPack_id: string
  user_id: string

  answer: string
  question: string
  grade: number
  shots: number

  questionImg: string
  answerImg: string
  answerVideo: string
  questionVideo: string

  comments: string

  type: string
  rating: number
  more_id: string

  created: string
  updated: string
}

export type AddCardsPackDataType = {
  cardsPack: {
    name?: string
    path?: string
    grade?: number
    shots?: number
    rating?: number
    deckCover?: string
    privat?: boolean
    type?: string
  }
}

export type UpdateCardsPackDataType = {
  cardsPack: {
    _id: string
    name: string
  }
}

export type PostCardDataType = {
  card: {
    cardsPack_id: string
    question: string // если не отправить будет таким
    answer: string // если не отправить будет таким
    type: 'card' // если не отправить будет таким
  }
}
