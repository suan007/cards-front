import React, { ChangeEvent, KeyboardEvent, useState } from 'react';

type AddItemFormPropsType = {
    addItem: (title: string, value: boolean, title2: string) => void
    disabled?: boolean
    placeholder: string
    isTwo?: boolean
}

export const AddItemForm = React.memo(function ({ addItem, disabled = false, placeholder, isTwo }: AddItemFormPropsType) {

    let [title, setTitle] = useState("")
    let [title2, setTitle2] = useState("")
    let [checkboxValue, setCheckboxValue] = useState(false)
    let [error, setError] = useState<string | null>(null)

    const addItemHandler = () => {
        if (title.trim() !== "") {
            addItem(title, checkboxValue, title2);
            setTitle("");
        } else {
            setError("Title is required");
        }
    }

    const onChangeHandler = (e: ChangeEvent<HTMLInputElement>) => {
        setTitle(e.currentTarget.value)
    }
    const onChangeHandler2 = (e: ChangeEvent<HTMLInputElement>) => {
        setTitle2(e.currentTarget.value)
    }
    const onChecked = (e: ChangeEvent<HTMLInputElement>) => {
        setCheckboxValue(e.currentTarget.checked)
    }

    const onKeyPressHandler = (e: KeyboardEvent<HTMLInputElement>) => {
        if (error !== null) {
            setError(null);
        }
        if (e.charCode === 13) {
            addItemHandler();
        }
    }

    return <div>

        <input type="text" value={title} placeholder={placeholder} onChange={onChangeHandler} onKeyPress={onKeyPressHandler} disabled={disabled} />
        {isTwo && <input type="text" value={title2} placeholder={placeholder} onChange={onChangeHandler2} onKeyPress={onKeyPressHandler} disabled={disabled} />}
        {placeholder === "add" && <input type="checkbox" checked={checkboxValue} onChange={onChecked} />}
        <button onClick={addItemHandler} disabled={disabled}> add
        </button>
    </div>
})
