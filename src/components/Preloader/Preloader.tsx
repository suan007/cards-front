import React from "react"
import spinner from "../../assets/Spinner.gif"

export const PreloaderSmall = () => {
    return (
        <div>
            <img src={spinner} alt="spinner" />
        </div>
    )
}