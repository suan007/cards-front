import React from "react";
import { useSelector } from "react-redux";
import { Redirect } from "react-router";
import { RoutePath } from "../app/App";
import { AppRootStateType } from "../app/store";


export const RedirectToLogin = () => {
    const logged = useSelector<AppRootStateType, boolean>(state => state.login.logged)
    if (!logged) {
        return <Redirect to={RoutePath.LOGIN} />
    }
    return <div></div>
}